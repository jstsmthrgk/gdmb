from setuptools import setup

setup(
  name="gdmb",
  version="0.0.1",
  packages=["gdmb"],
  install_requires=[
    "yt-dlp",
    "nextcord"
  ],
  entry_points={
    "console_scripts": [
      "gdmb = gdmb.__main__:main"
    ]
  }
)
