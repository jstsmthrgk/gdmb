from yt_dlp import YoutubeDL
from nextcord import Interaction, SlashOption
import nextcord
import sys

ytdl_format_options = {
    'format': 'bestaudio/best',
    'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}

def add_slash_command(cls, desc, opt):
    def _decorator(coro):
        cls._to_register.append((coro, desc, opt))
        return coro
    return _decorator

class Client(nextcord.Client):
    _to_register = []
    
    def __init__(self, guilds, stations, loops):
        super().__init__()
        self.guilds_ = guilds
        self.stations = stations
        self.loops = loops
        self.ytdl = YoutubeDL(ytdl_format_options)
        self.status_messages = {}
        self.register_slash_commands()

    async def on_ready(self):
        appinfo = await self.application_info()
        print(f"https://discord.com/oauth2/authorize?client_id={appinfo.id}&scope=bot+applications.commands&permissions=3145728", file=sys.stderr)

    async def connect_to_vc(self, member, voice_client):
        if member.voice and member.voice.channel:
            if voice_client and voice_client.channel and voice_client.channel == member.voice.channel:
                return True
            else:
                if voice_client:
                    await voice_client.disconnect()
                await member.voice.channel.connect()
                return True
        else:
            return False

    async def show_playing(self, guild, title):
        if guild.id in self.status_messages:
            await self.status_messages[guild.id].edit(
                embed=nextcord.Embed(
                    title="Ich spiele aktuell",
                    description=title
                )
            )
        else:
            status_channel = guild.get_channel(self.guilds_[guild.id])
            self.status_messages[guild.id] = await status_channel.send(
                embed=nextcord.Embed(
                    title="Ich spiele aktuell",
                    description=title
                )
            )

    def register_slash_commands(self):
        for cmd in self._to_register:
            cmd[0].__defaults__ = cmd[2](self)
            self.slash_command(guild_ids=self.guilds_.keys(), description=cmd[1])(cmd[0])
        
