from . import *
from pathlib import Path
import argparse
import csv
import os

def main():
    parser = argparse.ArgumentParser(description="Discord Musikbot, duh")
    parser.add_argument("-t", "--token-file", required=True, metavar="token.txt")
    parser.add_argument("-g", "--guild-list", required=True, metavar="guilds.csv")
    parser.add_argument("-s", "--station-list", metavar="stations.csv")
    parser.add_argument("-l", "--loop-directory", metavar="loops/")
    args = parser.parse_args()

    guilds = None
    with open(args.guild_list, newline='') as csvfile:
        guilds = {}
        csvreader = csv.reader(csvfile, delimiter='\t', escapechar=None, quoting=csv.QUOTE_NONE, strict=True)
        for entry in csvreader:
            guilds[int(entry[0])] = int(entry[1])

    stations = None
    if args.station_list is not None:
        stations = []
        with open(args.station_list, newline="") as csvfile:
            csvreader = csv.reader(csvfile, delimiter='\t', escapechar=None, quoting=csv.QUOTE_NONE, strict=True)
            for entry in csvreader:
                stations.append((entry[0], entry[1]))

    loops = None
    if (loopdir := args.loop_directory) is not None:
        if loopdir.endswith("/"): # TODO: muss das sein?
            loopdir = loopdir[:-1]
        loops = []
        for loop in os.listdir(loopdir):
            if loop.endswith(".opus"):
                loops.append((loop[:-5], str(Path(loopdir) / loop)))

    print(guilds)
    print(stations)
    print(loops)    

    client = Client(guilds, stations, loops)
    print(client._to_register)
    with open(args.token_file) as f:
        auth_token = f.readlines()[0].rstrip()
    client.run(auth_token)



if __name__ == "__main__":
  main()
