{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = {self, nixpkgs, utils}:
  let out = system:
  let pkgs = nixpkgs.legacyPackages."${system}";
      pythonPackages = pkgs.python3Packages;
      dependencies = with pkgs; [
        pythonPackages.nextcord
        pythonPackages.setuptools
        pythonPackages.yt-dlp
        ffmpeg
      ];
  in rec {
    devShell = pkgs.mkShell {
      buildInputs = dependencies;
    };
    packages.gdmb = pythonPackages.toPythonApplication (pythonPackages.buildPythonPackage rec {
      name = "gdmb";
      src = ./.;
      propagatedBuildInputs = dependencies;
    });
    defaultPackage = packages.gdmb;
 }; in with utils.lib; eachDefaultSystem out
 // {
  nixosModule = { config, lib, pkgs, ...}:
    with lib;
    let cfg = config.services.gdmb;
    in {
      options.services.gdmb = {
        enable = mkEnableOption "gdmb";
        tokenFile = mkOption {
          type = types.path;
          description = "Path to file containing Discord bot token";
        };
        guilds = mkOption {
          type = with types; attrsOf str;
          description = "GuildIDs of Discord guilds to join mapped to their status channel ids";
        };
        stations = mkOption {
          type = with types; attrsOf str;
          description = "URLs of radio stations to provide";
          default = {};
        };
        loopDirectory = mkOption {
          type = with types; nullOr path;
          description = "Directory with Loops to provide";
          default = null;
        };
      };
      config = mkIf cfg.enable (
        let
          guildsFile = pkgs.writeText "guilds.csv"
            ''${concatStringsSep "\n"
                  (attrsets.mapAttrsToList (guild: channel: "${guild}\t${channel}") cfg.guilds)}'';
          stationsFile = if cfg.stations == {} then null else pkgs.writeText "stations.csv"
            ''${concatStringsSep "\n"
                  (attrsets.mapAttrsToList (name: url: "${name}\t${url}") cfg.stations)}'';
        in {
          systemd.services.gdmb = {
            description = "Discord Music Bot";
            wantedBy = [ "multi-user.target" ];
            after = [ "network.target" ];
            script = ''
              ${self.packages."${config.nixpkgs.system}".gdmb}/bin/gdmb -t ${cfg.tokenFile} -g ${guildsFile} ${optionalString (stationsFile != null) ''-s ${stationsFile}''} ${optionalString (cfg.loopDirectory != null) ''-l ${cfg.loopDirectory}''}
            '';
          };
        }
      );
    };
  };
}
